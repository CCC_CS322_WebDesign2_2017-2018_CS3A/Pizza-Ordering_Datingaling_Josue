calculatePrice = function calculatePrice() {
  var resultmessage = "";
  var pizzamount = parseFloat(0);
  var radval;
  var radval2;
  var chckbox;
  var totalValue = parseInt(0);
  var priceTable = [
    {
      size: "8",
      price: 9.25
    }, 
	{
      size: "10",
      price: 10.75
    }, 
	{
      size: "15",
      price: 12.00
    }
	];
	
	var priceCrust = [
    {
      price: 7.25
    }, 
	{
      price: 4.75
    }
	];
  var size = document.getElementsByName("size");
  var crust = document.getElementsByName("crust");
  var addons = document.getElementsByName("addons");
  var toppingss = document.getElementsByName("toppingss");
  
  for (var i=0; i<size.length; i++) {
    if(size[i].checked){
    radVal = priceTable[i].size;
    totalValue += priceTable[i].price;
    }
  }
  for (var i=0; i<crust.length; i++) {
    if(crust[i].checked){
    radVal2 = priceCrust[i].crust;
    totalValue += priceCrust[i].price;
    }
  }
  for (var i=0; i<addons.length; i++) {
    if (addons[i].checked) {
      totalValue += (150/100);
    }
  }
  for (var i=0; i<toppingss.length; i++) {
    if (toppingss[i].checked) {
      totalValue += (250/100);
    }
  }
  
  totalValue = parseFloat(totalValue);
  var resultmessage = "Total cost: $" + totalValue;
  document.getElementsByClassName("running-total")[0].innerHTML = resultmessage;

}